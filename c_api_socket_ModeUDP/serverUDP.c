#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>

#define PORT 12345

// Structure pour stocker les informations du client
struct ClientInfo {
    pid_t pid;
    struct sockaddr_in client_addr;
    socklen_t addr_len;
};

int main() {
    int sock;
    struct sockaddr_in local;
    socklen_t local_len;

    // Créer une socket de type SOCK_DGRAM pour UDP
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // Initialiser les informations sur le serveur
    bzero(&local, sizeof(local));
    local.sin_family = AF_INET;
    local.sin_port = htons(PORT);
    local.sin_addr.s_addr = INADDR_ANY;

    // Lier la socket à l'adresse et au port spécifiés
    if (bind(sock, (struct sockaddr *)&local, sizeof(local)) == -1) {
        perror("bind");
        exit(EXIT_FAILURE);
    }

    // Liste pour stocker les informations des clients
    struct ClientInfo clients[10]; // Ajustez la taille selon vos besoins

    while (1) {
        char request[256];
        char response[256];

        struct sockaddr_in client;
        socklen_t client_len = sizeof(client);

        // Recevoir des données du client en utilisant recvfrom pour UDP
        ssize_t bytes_received = recvfrom(sock, request, sizeof(request), 0, (struct sockaddr *)&client, &client_len);
        if (bytes_received == -1) {
            perror("recvfrom");
            exit(EXIT_FAILURE);
        }

        // Ajouter un caractère de fin de chaîne pour imprimer correctement la chaîne
        request[bytes_received] = '\0';

        // Afficher le message du client
        printf("Message from client: %s\n", request);

        // Vérifier si le message est "fin"
        if (strcmp(request, "fin") == 0) {
            printf("Client has closed the connection.\n");

            // Rechercher le client dans la liste en utilisant son adresse
            int i;
            for (i = 0; i < sizeof(clients) / sizeof(clients[0]); i++) {
                if (clients[i].addr_len == client_len && memcmp(&clients[i].client_addr, &client, client_len) == 0) {
                    // Terminer le processus fils correspondant au client
                    printf("Terminating child process for client %d.\n", clients[i].pid);
                    kill(clients[i].pid, SIGTERM);
                    break;
                }
            }

            continue;  // Ignorer le reste du traitement pour ce client
        }

        // Créer un processus fork
        pid_t pid = fork();

        if (pid == -1) {
            perror("fork");
            exit(EXIT_FAILURE);
        } else if (pid == 0) {  // Processus enfant
            // Lire la réponse du serveur depuis la console
            printf("Enter your response: ");
            fgets(response, sizeof(response), stdin);

            // Envoyer la réponse au client en utilisant sendto pour UDP
            ssize_t bytes_sent = sendto(sock, response, strlen(response), 0, (struct sockaddr *)&client, client_len);
            if (bytes_sent == -1) {
                perror("sendto");
                exit(EXIT_FAILURE);
            }

            // Fermer la socket dans le processus enfant
            close(sock);

            exit(EXIT_SUCCESS);
        } else {  // Processus parent
            // Ajouter le client à la liste
            int i;
            for (i = 0; i < sizeof(clients) / sizeof(clients[0]); i++) {
                if (clients[i].pid == 0) {
                    clients[i].pid = pid;
                    clients[i].client_addr = client;
                    clients[i].addr_len = client_len;
                    break;
                }
            }
        }
    }

    // Fermer la socket dans le processus parent
    close(sock);

    return 0;
}
