#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>

#define SERVER_IP "127.0.0.1"
#define PORT 12345

int main() {
    int sock;
    struct sockaddr_in serv_addr;

    // Créer une socket de type SOCK_DGRAM pour UDP
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // Initialiser les informations sur le serveur
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    // Convertir l'adresse IP de la chaîne en format binaire
    if (inet_pton(AF_INET, SERVER_IP, &(serv_addr.sin_addr)) <= 0) {
        perror("inet_pton");
        exit(EXIT_FAILURE);
    }

    while (1) {
        char message[256];
        char response[256];

        // Lire le message du client depuis la console
        printf("Enter your message: ");
        fgets(message, sizeof(message), stdin);

        // Envoyer le message au serveur en utilisant sendto pour UDP
        ssize_t bytes_sent = sendto(sock, message, strlen(message), 0, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
        if (bytes_sent == -1) {
            perror("sendto");
            exit(EXIT_FAILURE);
        }

        if (strcmp(message, "fin\n") == 0) {
            printf("Client has closed the connection. Exiting.\n");
            break;
        }

        // Recevoir la réponse du serveur
        socklen_t addr_len = sizeof(serv_addr);
        ssize_t bytes_received = recvfrom(sock, response, sizeof(response), 0, (struct sockaddr *)&serv_addr, &addr_len);
        if (bytes_received == -1) {
            perror("recvfrom");
            exit(EXIT_FAILURE);
        }

        // Ajouter un caractère de fin de chaîne pour imprimer correctement la chaîne
        response[bytes_received] = '\0';

        // Afficher la réponse du serveur
        printf("Message from server: %s\n", response);
    }

    // Fermer la socket lorsque vous avez terminé
    close(sock);

    return 0;
}
