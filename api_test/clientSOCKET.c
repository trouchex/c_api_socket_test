#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <strings.h>
#include <unistd.h>
#include <netdb.h>

#define SERV "127.0.0.1"
#define PORT 12345

int main() {
    int port, sock;
    struct sockaddr_in serv_addr;
    struct hostent *serveur;

    port = PORT;
    serveur = gethostbyname(SERV);

    if (!serveur) {
        fprintf(stderr, "Problème serveur \"%s\"\n", SERV);
        exit(1);
    }

    sock = socket(AF_INET, SOCK_STREAM, 0);
    bzero(&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy(serveur->h_addr, &serv_addr.sin_addr.s_addr, serveur->h_length);
    serv_addr.sin_port = htons(port);

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        perror("Connexion impossible");
        exit(1);
    }

    // Boucle de communication
    // Boucle de communication
    while (1) {
        // Envoyer un message au serveur
        char message[256];
        printf("Enter your message: ");
        fgets(message, sizeof(message), stdin);

        // Envoyer le message au serveur
        send(sock, message, sizeof(message), 0);


        // Recevoir une réponse du serveur
        char response[256];
        ssize_t bytes_received = recv(sock, response, sizeof(response), 0);

        if (bytes_received <= 0) {
            // Si la connexion est fermée par le serveur, sortir de la boucle
            printf("Server has closed the connection. Exiting.\n");
            break;
        }

        printf("Message from server: %s\n", response);
    }
    // Fermer la connexion avec le serveur
    close(sock);

    return 0;
}
