#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <strings.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PORT 12345

int main() {
    int sock, sock2, lg;
    struct sockaddr_in local;
    struct sockaddr_in distant;

    local.sin_family = AF_INET;
    local.sin_port = htons(PORT);
    local.sin_addr.s_addr = INADDR_ANY;
    bzero(&(local.sin_zero), 8);
    lg = sizeof(struct sockaddr_in);

    sock = socket(AF_INET, SOCK_STREAM, 0);
    bind(sock, (struct sockaddr *)&local, sizeof(struct sockaddr));
    listen(sock, 5);

    while (1) {
        sock2 = accept(sock, (struct sockaddr *)&distant, &lg);

        // Boucle de communication
        while (1) {
            // Recevoir un message du client
            char message[256];
            ssize_t bytes_received = recv(sock2, message, sizeof(message), 0);

            // Vérifier si le message est "fin"
            // Vérifier si le message est "fin"
            if (strcmp(message, "fin\n") == 0) {
                printf("Fin de la conversation\n");
                close(sock); // Close the connection with the specific client
                // Optionally, you can break out of the communication loop to stop processing messages from this client
                break;
            }


            if (bytes_received <= 0) {
                // Si la connexion est fermée par le client, sortir de la boucle
                printf("Client has closed the connection. Exiting.\n");
                close(sock);
                exit(0);
            }

            printf("Message from client: %s\n", message);


            // Répondre au client
            char response[256];
            printf("Enter your response: ");
            fgets(response, sizeof(response), stdin);
            send(sock2, response, sizeof(response), 0);
        }

        // Fermer la connexion avec le client
        close(sock2);
        break;
    }

    close(sock);
    return 0;
}
